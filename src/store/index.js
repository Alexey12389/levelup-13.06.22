// import axios from 'axios'
import { createStore } from 'vuex'

export default createStore({
  state: {
    products: [{
      image: '1.png',
      name: 'Converse kids 70',
      price: 49.99,
      article: 'T1',
      discount: 'full price',
      category: 'sneaker'
    },
    {
      image: '2.png',
      name: 'Converse Chuck 70',
      price: 49.99,
      article: 'T2',
      discount: 'full price',
      category: 'sneaker'
    },
    {
      image: '3.png',
      name: 'Converse Chuck 70 Renew High Top',
      price: 50.99,
      article: 'T3',
      discount: 'bestsellers',
      category: 'sneaker'
    },
    {
      image: '4.png',
      name: 'Converse Pro Chuck 80',
      price: 64.99,
      article: 'T4',
      discount: 'bestsellers',
      category: 'sneaker'
    },
    {
      image: '5.png',
      name: 'Converse Winter Chuck 80',
      price: 79.99,
      article: 'T5',
      discount: 'bestsellers',
      category: 'sneaker'
    },
    {
      image: '6.png',
      name: 'Converse Winter Chuck 70 Full Black',
      price: 129.99,
      article: 'T6',
      discount: 'full price',
      category: 'sneaker'
    },
    {
      image: '7.png',
      name: 'Converse Winter Chuck 70 Black/White',
      price: 99.99,
      article: 'T7',
      discount: 'full price',
      category: 'no-sneaker'
    }],
    cart: [],
    filteredProducts: [],
    searchValue: '',
    result: []
  },
  getters: {
    productsItems: (state) => state.filteredProducts,
    SEARCH_VALUE (state) {
      return state.searchValue
    },
    PRODUCTS (state) {
      return state.products
    },
    CART (state) {
      return state.cart
    },
    totalPrice: (state) => {
      const total = state.cart.reduce((acc, item) => {
        return acc + item.price
      }, 0)
      return total.toFixed(2)
    }
  },
  mutations: {
    SET_SEARCH_VALUE_TO_VUEX: (state, value) => {
      state.searchValue = value
    },
    SET_PRODUCTS_TO_STATE: (state, products) => {
      state.products = products
    },
    SET_CART: (state, product) => {
      state.cart.push(product)
    },
    REMOVE_FROM_CART: (state, index) => {
      state.cart.splice(index, 1)
    },
    REMOVE_ALL_BASKET (state) {
      state.cart = []
    },
    FILTER_ITEMS (state, payload) {
      let filtered = state.products
      if (!payload) {
        return filtered
      }
      let index = 0
      while (index < payload.length) {
        const filterItem = payload[index]
        const filterKey = Object.keys(filterItem)[0]
        filtered = filtered.filter(
          (i) => i[filterKey] === filterItem[filterKey]
        )
        index++
      }
      state.filteredProducts = filtered
    },
    REMOVE_BASKET (state, payload) {
      const index = state.cart.findIndex(
        (e) => e.article === payload.article
      )
      state.cart.splice(index, 1)
    }
  },
  actions: {
    GET_SEARCH_VALUE_TO_VUEX ({ commit }, value) {
      commit('SET_SEARCH_VALUE_TO_VUEX', value)
    },
    ADD_TO_CART ({ commit }, product) {
      commit('SET_CART', product)
    },
    DELETE_FROM_CART ({ commit }, index) {
      commit('REMOVE_FROM_CART', index)
    },
    filter ({ commit }, filter) {
      commit('FILTER_ITEMS', filter)
    },
    removeBasketItem ({ commit }, item) {
      commit('REMOVE_ALL_BASKET', item)
    }
  },
  modules: {
  }
})
