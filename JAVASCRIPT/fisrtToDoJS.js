class Model {
    constructor() {
        this.todos =JSON.parse(localStorage.getItem('todos')) || [];
        this.filterValue = {
            all: 'All',
            completed: 'Completed'
        }
        this.filter = this.filterValue.all
    }

    bindTodoListChanged (callback) {
        this.onTodoListRender = callback
    }

    updateRender(todos = this.todos) {
        const filteredArray = todos.filter((item) => {
            if(this.filter === this.filterValue.all) return true;
            return this.filter === this.filterValue.completed ? item.completed : !item.completed
        })
        this.onTodoListRender(filteredArray);
        localStorage.setItem('todos', JSON.stringify(this.todos));
    }

    addTodo (text) {
        const todo = {
            id: Date.now(),
            title: text,
            completed: false
        };

        this.todos.push(todo);
        this.updateRender(this.todos)
    };

    removeTodo(todoId) {
        this.todos = this.todos.filter((item) => item.id !== todoId);
        this.updateRender(this.todos);
    };

    editToggleCheckbox(todoId) {
        this.todos = this.todos.map((item) => item.id === todoId ? {...item, completed: !item.completed}: item);
        this.updateRender(this.todos);
    };

    editTodoText(todoId, updatedText) {
        this.todos = this.todos.map((item) => item.id === todoId ? {...item, title: updatedText} : item);
        this.updateRender(this.todos);
    };

    filteredTodo(selectedOptionValue) {
        this.filter = selectedOptionValue;
        this.updateRender();
    }
}

class View {
    constructor() {
        this.app = this.getElement('#root');
        this.form = this.createElement("form");
        this.header = this.createElement('header', "header")
        this.select = this.createElement('select', 'select__filter')
        this.input = this.createElement("input", 'form__input');
        this.todoList = this.createElement('ul', "lists")
        this.buttonAdd = this.createElement('button', "form__button")

        this.options = [
            {value:'All', text: 'All'},
            {value:'Completed', text: 'Completed'},
            {value:'Nocompleted', text: 'No completed'}
        ];


        for (let i = 0; i < this.options.length; i++) {
            const opt = this.createElement('option');
            opt.value = this.options[i].value;
            opt.innerHTML = this.options[i].text;
            this.select.appendChild(opt);
        }

        this.input.placeholder = 'add todo'
        this.input.type = 'text'
        this.buttonAdd.textContent = "добавить"

        this.header.append(this.select)
        this.form.append(this.input, this.buttonAdd);
        this.app.append(this.header, this.form, this.todoList);

        this.editInput = '';
        this.initLocalValue();
    };

    get _todoText() {
        return this.input.value;
    };

    resetInput() {
        this.input.value = ''
    };

    renderTodo(todos) {
        console.log('todos', todos)

        while(this.todoList.firstChild) {
             this.todoList.removeChild(this.todoList.firstChild);
        }

        if (!todos.length) {
            const p = this.createElement('p', "title__hind");
            p.textContent = 'Not ToDos!';
            this.todoList.append(p);
            console.log(todos)
        } else {
            todos.map((item) => {
                const li = this.createElement('li', 'list');
                const span = this.createElement('span');
                const checkbox = this.createElement('input');
                const buttonDelete = this.createElement('button', "list__button");

                checkbox.type = 'checkbox'
                checkbox.checked = item.completed
                buttonDelete.textContent = 'удалить';

                li.id = item.id;
                span.contentEditable = true;
                span.classList.add('editable');

                if(item.completed) {
                    const stricke = this.createElement('s');
                    stricke.textContent = item.title;
                    span.append(stricke);
                } else {
                    span.textContent = item.title
                }

                li.append(checkbox, span, buttonDelete);
                this.todoList.append(li);
            })
        }
    };

    initLocalValue() {
        this.todoList.addEventListener('input', event => {

            if(event.target.className === 'editable') {
                this.editInput = event.target.innerText;
            }

        });
    };

    bindAddTodo(handler) {
        this.form.addEventListener('submit', (event) =>{
            event.preventDefault();
            if(this._todoText) {
                handler(this._todoText);
                this.resetInput();
            }
        })
    };

    bindRemoveTodo(handler) {
        this.todoList.addEventListener("click", (event) =>{
            if (event.target.className === 'list__button') {
                handler(+event.target.parentElement.id)
            }
        })
    };

    bindToggleCheckbox(handler) {
        this.todoList.addEventListener('change', (event) => {
            if (event.target.type === 'checkbox')
            handler(+event.target.parentElement.id)
        })
    };

    bindEditTodo(handler) {
        this.todoList.addEventListener('focusout', event => {
            if(this.editInput) {
                handler(+event.target.parentElement.id, this.editInput);
                this.editInput = '';
            }
        })
    };

    bindFilterTodo(handler) {
        this.select.addEventListener('click', () => {
            const selectedOptionValue = this.select.options[this.select.selectedIndex].value
            handler(selectedOptionValue);
        })
    }

    createElement(tag, className) {
        const element = document.createElement(tag);

        if(className) element.classList.add(className);

        return element;
    };

    getElement(selector) {
        const element = document.querySelector(selector);
        return element;
    };
}

class Controller {
    constructor (model, view) {
        this.model = model;
        this.view = view;
        this.model.bindTodoListChanged(this.onTodoListRender);
        this.view.bindAddTodo(this.handleAddTodo);
        this.view.bindRemoveTodo(this.handleRemoveTodo);
        this.view.bindToggleCheckbox(this.handleEditToggle);
        this.view.bindEditTodo(this.handleEditTodo);
        this.view.bindFilterTodo(this.handleFilterTodo);
        this.onTodoListRender(this.model.todos);

    };
    onTodoListRender = (todos) => {
        this.view.renderTodo(todos);
    };
    handleAddTodo = (todoText) => {
        this.model.addTodo(todoText);
    };
    handleRemoveTodo = (todoId) => {
        this.model.removeTodo(todoId);
    };
    handleEditToggle = (todoId) => {
        this.model.editToggleCheckbox(todoId)
    };
    handleEditTodo = (todoId, todoText) => {
        this.model.editTodoText(todoId, todoText)
    };
    handleFilterTodo = (selectedOptionValue) => {
        this.model.filteredTodo(selectedOptionValue);
    };
}

const app = new Controller(new Model(), new View());