import EnemyController from "./EnemyController.js";
import Player from "./Player.js";
import BulletController from "./BulletController.js";
import Score from "./score.js";

const canvas = document.getElementById("game");
const ctx = canvas.getContext("2d");

canvas.width = 900;
canvas.height = 600;

const background = new Image();
background.src = "images/space.png";

const playerBulletController = new BulletController(canvas, 4, "red", true);
const enemyBulletController = new BulletController(canvas, 10, "yellow", false);
const enemyController = new EnemyController(
  canvas,
  enemyBulletController,
  playerBulletController,
);
const player = new Player(canvas, 3, playerBulletController);
const gameScore = new Score(canvas);

let isGameOver = false;
let didWin = false;
let score = 0;
// let GamePaused = false;

function game() {
  // pause();
  checkGameOver();
  ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
  displayGameOver();
  if (!isGameOver) {
    enemyController.draw(ctx);
    player.draw(ctx);
    playerBulletController.draw(ctx);
    enemyBulletController.draw(ctx);
    gameScore.drawScore(ctx);
  }
}

// function pause () {
//   if (GamePaused) {
//     GamePaused = false;
//     update()
//   }
//   else{
//     GamePaused = true;
//   }

// }

// function update(){
//   if (GamePaused){
//     ctx.font = "25pt Helvetica";
//     ctx.fillText("Game paused!", CANVAS_WIDTH/3, CANVAS_HEIGHT/2);
//   }
//   else{
//     ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
//     NEXT_ANIMATION_FRAME = setTimeout(update, 1000/60);
//   }
// }

function drawScore (ctx) {
  ctx.font = '16 Arial';
  ctx.fillStyle = '#0095DD';
  ctx.fillText("Score: "+score, 10, 10);
}

function displayGameOver() {
  if (isGameOver) {
    let text = didWin ? "You Win!" : "Game Over!";
    let textOffset = didWin ? 3.5 : 8;

    ctx.fillStyle = "white";
    ctx.font = "70px Arial";
    ctx.fillText(text, canvas.width / textOffset, canvas.height / 2);
  }
}

function checkGameOver() {
  if (isGameOver) {
    return;
  }

  if (enemyBulletController.collideWith(player)) {
    isGameOver = true;
  }

  if (enemyController.collideWith(player)) {
    isGameOver = true;
  }

  if (enemyController.enemyRows.length === 0) {
    didWin = true;
    isGameOver = true;
  }
}

setInterval(game, 1000 / 60);
