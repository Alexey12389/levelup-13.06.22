
import React, {FC} from "react";
import { useForm, Controller } from "react-hook-form";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useAppDispatch } from "../../header/store/hook";
import { addCategory } from "../../header/store/category/index";

let schema = yup.object().shape({
    category:yup.string().required("Поле не должно быть пустым")
  });

type CategoryForm = {
    category:string;
};

type AddCategoryProps = {
    todoId:number;
}


const AddCategory: FC<AddCategoryProps> = ({todoId}) => {
    const { control, handleSubmit,setValue, formState:{errors} } = useForm({
        defaultValues: {
            category: "",
        } as CategoryForm,
        resolver:yupResolver(schema)
      });

      const dispatch = useAppDispatch();


      const onSubmit = (data: CategoryForm) => {
        dispatch(addCategory({...data, todoId, id: Date.now()}))
        setValue("category",'');
    }


    return     <div>
    <form onSubmit={handleSubmit(onSubmit)}>
      <Controller
        name="category"
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            helperText={errors?.category?.message}
            error={!!errors?.category}
            size="small"
            id="outlined-basic"
            label="Добавить подкатегорию"
            variant="outlined"
          />
        )}
      />

      <Button variant="contained" type="submit">
        Добавить подкатегорию
      </Button>
    </form>
  </div>
}

export default AddCategory;