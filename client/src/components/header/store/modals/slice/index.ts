import { createSlice } from '@reduxjs/toolkit';
import ModalEditCategory from '../../../../Modals/ModalsEditCategory/ModalEditCategory';

type modalType ={
    name: modalsEnums;
    category?: string;
    categoryId?: number;
};

export enum modalsEnums  {
    modalDeleteCategory = 'modalDeleteCategory',
    modalEditCategory = 'ModalEditCategory'
}

type initialStateType = {
    modals: modalType[];
};
const initialState: initialStateType = {
    modals: [],
};

export const modalsSlice = createSlice({
    name: 'modals',
    initialState,
    reducers: {
        showModal: (state, { payload }) => ({
            ...state, modals: [...state.modals, payload]
        }),
        hideModal: (state, { payload }) => ({
            ...state, modals: state.modals.filter((item) => item.name !== payload.name)
        })
            
    },
});

export const { showModal, hideModal } = modalsSlice.actions

export default modalsSlice.reducer
