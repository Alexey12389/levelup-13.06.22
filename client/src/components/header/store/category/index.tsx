import { createSlice } from '@reduxjs/toolkit';


type CategoryType = {
    category: string;
    id: number;
    todoId: number;
    description?: string;
    completed: boolean;
}

interface ICategoryState {
    category: CategoryType[]
}

const initialState: ICategoryState = {
    category: [{ category: 'Title', id: 2, todoId: 2, description: '', completed: false }]
}

export const categorySlice = createSlice({
    name: 'category',
    initialState,
    reducers: {
        addCategory: (state, { payload }) => ({
            ...state, category: [...state.category, { ...payload, description: '', completed: false }]
        }),
        removeCategory: (state, { payload }) => ({ ...state, category: state.category.filter((item) => item.id !== payload) }),
        editCompletedCategory: (state, { payload }) => ({
            ...state, category: state.category.map((item) => item.id === payload.categoryId ? { ...item, completed: !item.completed } : item)
        }),

        transferCategory: (state, { payload }) => ({
            ...state, category: state.category.map((item) => item.id === payload.categoryId ? { ...item, todoId: payload.todoId } : item)
        }),
        editCategory: (state, { payload }) => ({ ...state, category: state.category.map((item) => item.id === payload.categoryId ? { ...item, ...payload.categoryForm } : item) })

    },

})

export const {addCategory, removeCategory, editCompletedCategory, transferCategory, editCategory} = categorySlice.actions

export default categorySlice.reducer