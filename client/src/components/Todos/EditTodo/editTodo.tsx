import { Box, Button, TextField } from "@mui/material";
import React, { BaseSyntheticEvent, FC, useState } from "react";
import { useAppDispatch } from '../../header/store/hook';
import { updateTodo } from "../../header/store/todo/slice";
import styles from './editTodo.module.scss'

type EditTodoProps = {
    title?: string;
    todoId: number;
}
const EditTodo: FC<EditTodoProps> = ({ title, todoId }) => {
    const [value, setValue] = useState(title);
    const [error, setError] = useState('');

    const dispatch = useAppDispatch();

    const handleOnchangeInput = (e: BaseSyntheticEvent) => {
        setValue(e.target.value)
    };

    const handleEditTodo = () => {
        const obj = {
            title: value,
            todoId,
        }
        if(!value) {
           return setError('input ну должен быть пустым')
        }
        setError('')
        dispatch(updateTodo(obj))
    }

    return <Box className={styles.box}>
        <TextField error={!!error} helperText= {error} value={value} onChange={handleOnchangeInput} size="small" id="outlined-basic" label="Редактировать запись" variant="outlined" />
        <Button variant="contained" onClick={handleEditTodo}>Редактировать</Button>
    </Box>
}

export default EditTodo;
