import React, { BaseSyntheticEvent, useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import styles from './AddTodo.module.scss';
import { useAppDispatch } from '../../header/store/hook';
import { addTodo } from '../../../components/header/store/todo/slice';
import { useForm, Controller } from "react-hook-form";

type AddTodoForm = {
  title: string;
}

let schema = yup.object().shape({
  title: yup.string().required("поле должно быть обязательным")
})

export const AddTodo = () => {


  const { control, handleSubmit, setValue, formState: { errors } } = useForm({
    defaultValues: {
      title: ''
    },
    resolver: yupResolver(schema)
  });

  const dispatch = useAppDispatch();
  const onSubmit = (data: AddTodoForm) => {
    dispatch(addTodo({ ...data, id: Date.now() }))
    setValue("title", ''); 
  }


  return (
    <div className={styles.wrapper}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Controller
          name="title"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              error={!!errors?.title}
              helperText={errors?.title?.message}
              size="small"
              id="outlined-basic"
              label="Добавить запись"
              variant="outlined"
            />
          )}
        />

        <Button variant="contained" className={styles.button} type="submit">
          Добавить
        </Button>
      </form>
    </div>
  );
};


export default AddTodo;
